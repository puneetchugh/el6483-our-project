Project Proposal for EL6483
==========================

## Team

Team members (up to four). Write name and email address of each team member:


1.Sagar Deo(sd2602) 

2.Krishnan Ganesh(kg1682)

3.Puneet Chugh(pc1837)

4.N/A

## Idea

We are proposing to make a heads-up display with which you can see a video without having to change your usual viewpoint. The video will be loaded on a SD card which will be interfaced with the microcontroller. The STM-board will in turn decode the video stored on SD card in different formats and feed it to the head-mount, that will display the video.

The head-mount assembly will contain a semi-transparent mirror, a projection lens and a TFT display. The basic idea is to project microcontroller board interfaced TFT display onto the semi-transparent mirror using the lens.

However, Oculus-Rift and other head mounts that display video come at a huge cost, typically above $300. This served as the major motivation to come out with our own version of head-mount video player that comes at a smaller price. So, this might be considered as a poor man's "Oculus Rift"

## Materials

The materials that would be used in this project is:
1) STM board

2) SD card - This would store the videos

3) SD card slot - This would be used for interfacing the SD card with the board.

4) TFT screen - This screen would display the videos stored on SD card

## Milestones

Milestones mark specific points along a project timeline. Your project should have two milestones, and you should plan to demonstrate a working prototype at each:

* 16 April 2015: Play the video on the TFT screen. This is the major step.

* 7 May 2015: The project would be fully functional by this time. Not only it will play video, but also the design and construction of the head-gear would be ready, that would display the video content on the screen.

## Plan of work

The plan of the work has been divided into some important steps:

ABOUT RTOS
a) Figure out if we need the RTOS or not?
b) Figure out how our code interacts with the RTOS environment.
c) Figure out whether RTOS require external memory card to run or the board is sufficient.
d) Can we write our own code or do we need RTOS?
e) ChibiOS and Free RTOS are the ones to be researched.

Sagar will work on a) and b)

Puneet c) and d)

Krishnan b) and e)

FUNCTIONS 

a) Read the data from the SD card using SPI - Sagar Deo 

b) Decode the video stored on SD - Puneet Chugh

c) Finally play the video on the screen - Krishnan Ganesh


HARDWARE - it will be done together to ensure it comes out to be the best possible design. It has two considerations to it:

a) Size of the memory card that the board can support.

b) design and assembly of the head-mount

The report is supposed to be completed for a particular function by the person concerned with the function.


