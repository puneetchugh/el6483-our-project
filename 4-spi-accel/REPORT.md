Lab 4: Accelerometer and SPI
============================

Lab Assignment | 4 - Accelerometer
-------------- | -----------------------
Name           | Sagar Deo
Net ID         | sd2604
Report due     | Wednesday, 11 March 2015


Please answer the following questions:

1) Which GPIO pins are used by the MEMS accelerometer for each
of the four SPI lines? (Refer to page 40 of the
[user manual](http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/DM00039084.pdf)
for the STM32F4 Discovery board.)

SPI Line         | Port and pin number
---------------- | --------------------
SCK (clock)      | Port A Pin 5
MOSI             | Port A Pin 7
MISO             | Port A Pin 6
CS (chip select) | Port E Pin 3


2) Read the `initAccelerometer()` function. Explain in your own words
how the SPI interface is configured and used for communication with the accelerometer.

The SPI interface is configured to communicate with the accelerometer by following the following steps:

a) The clock is initialized for both the SPI and GPIO peripherals.
b) GPIOA is initialized with the help of a library function and gpio_spi1 structure.
c) Alternate function of the GPIOA pins are defined.
d) The micro controller is configured as a master with the help of a spi structure and library
   function.
e) GPIOE pin 3 is configured to work as a slave select line and its value is set.
f) The MEMS registers are set and their values assigned using the definitions in main.h header file.

3) What are your zero-G offsets in the X, Y, and Z directions?
Show 10 lines of output from your board when it is
sitting on a flat surface. Then fill in the table.


```

X: 20 Y: -6 Z: 1051
X: 20 Y: -5 Z: -6
X: 19 Y: -5 Z: 1051
X: 20 Y: -5 Z: -7
X: 18 Y: -6 Z: 1051
X: 19 Y: -4 Z: 1052
X: 19 Y: -6 Z: 1051
X: 19 Y: -5 Z: 1051
X: 19 Y: -6 Z: 1051
X: 21 Y: -5 Z: 1051
X: 20 Y: -4 Z: 1051

```

Axis         | Offset
------------ | --------------------
X            | 20
Y            | -5
Z            | 51

4) Show a screenshot of your `gdb` window at the first step
_after_ the CS pin is set to low.

![CS pin low](http://imgur.com/ur2vfJM)

5) Show the code you use to light the LEDs according to the board's orientation.

```

while(1) {
  if (millisecondCounter > lastTime + 100) {
    readAxes(&dat);	// read sensor and store into `dat'
    setbuf(stdout, NULL);
    printf("X: %d Y: %d Z: %d\n", dat.X, dat.Y, dat.Z);	// the member variables for each direction
    lastTime = millisecondCounter;


    if (dat.X >=1000){
      GPIO_SetBits(GPIOD, GPIO_Pin_14);
    }
    else if(dat.Z >= 500){GPIO_ResetBits(GPIOD, GPIO_Pin_14);};
    //else {GPIO_ResetBits(GPIOD, GPIO_Pin_14);}

    if (dat.X <= -1000){
      GPIO_SetBits(GPIOD, GPIO_Pin_12);
    }
    else if(dat.Z >= 500){GPIO_ResetBits(GPIOD, GPIO_Pin_12);};
    //else {GPIO_ResetBits(GPIOD, GPIO_Pin_12);}

    if (dat.Y >=1000){
      GPIO_SetBits(GPIOD, GPIO_Pin_13);
    }
    else if(dat.Z >= 500){GPIO_ResetBits(GPIOD, GPIO_Pin_13);};
    //else {GPIO_ResetBits(GPIOD, GPIO_Pin_13);}

    if (dat.Y <= -1000){
      GPIO_SetBits(GPIOD, GPIO_Pin_15);
    }
    else if(dat.Z >= 500){GPIO_ResetBits(GPIOD, GPIO_Pin_15);};
    //else {GPIO_ResetBits(GPIOD, GPIO_Pin_15);}

    // Add extra logic here to light LEDs based on orientation of the board

  }
```


6) In the `accel-whoami` program, we are reading from a register
on the device that is configured to return a default value. Based on the value
that is returned, we can identify the device as a LIS3DSH or a LIS302DL.

The SPI Read protocol is described in Section 6.2.1 (Figure 7) of the
[LIS3DSH datasheet](http://www.st.com/web/en/resource/technical/document/datasheet/DM00040962.pdf).
Create a timing diagram showing exactly what happens (on all four lines) when you call
`readSPI(LIS3DSH_WHO_AM_I_ADDR)`. (The register addresses are defined in `main.h`
and are also given in Table 16 of the
[LIS3DSH datasheet](http://www.st.com/web/en/resource/technical/document/datasheet/DM00040962.pdf).)
Assume that the accelerometer returns the default value for this register, which is
given in Section 8.3 of the datasheet.

Insert an image of your timing diagram here.

![timing diagram](http://imgur.com/NftrPoQ)
